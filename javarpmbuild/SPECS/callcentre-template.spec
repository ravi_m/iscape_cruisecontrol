Summary: The iScape thomson and callcentre webapps
Name: callcentre
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/thomson
Prefix: /tui/iscape/releases

%description
The thomson and callcentre webapps provide web pages to search and book dynamically packaged holiday components. 

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
# not including thomson webapp yet
#get /pub/artifacts/webdev-webapps-thomson/@cruisebuilddate@/thomson.conf thomson.conf
#get /pub/artifacts/webdev-webapps-thomson/@cruisebuilddate@/thomson.war thomson.war
get /pub/artifacts/callcentre/@cruisebuilddate@/callcentre.conf callcentre.conf
get /pub/artifacts/callcentre/@cruisebuilddate@/callcentre.war callcentre.war
bye

svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/publish rpmbuild/publish
svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
#cp thomson.war $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
#cp thomson.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp callcentre.war $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
cp callcentre.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/tomcat.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish
cp rpmbuild/publish/conf/catalina.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/server.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/tomcat-users.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/web.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/iScape/localhost/host-manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
#cp rpmbuild/publish/conf/iScape/localhost/thomson.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/callcentre.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/logging/classes/log4j.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
cp rpmbuild/publish/logging/lib/commons-logging-1.1.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
cp rpmbuild/publish/logging/lib/log4j-1.2.13.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
cp rpmbuild/deployscripts/deploythomson.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/deploycallcentre.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
chmod +x /tui/iscape/releases/is@version@.@release@/deployscripts/*
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT 

%files
%defattr(-,iscape,ttguser)

#/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts/thomson.war
#/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/thomson.conf
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts/callcentre.war
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/callcentre.conf
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/catalina.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/server.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/tomcat-users.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/web.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/host-manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/manager.xml
#/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/thomson.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/callcentre.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes/log4j.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/commons-logging-1.1.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/log4j-1.2.13.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
/tui/iscape/releases/is@version@.@release@/deployscripts/deploythomson.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/deploycallcentre.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh

%changelog
* @rpmbuilddate@ Bob Builder <Julia_Dain@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
