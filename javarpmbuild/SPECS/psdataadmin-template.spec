Summary: The Powersearch Data Admin program
Name: psdataadmin
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/psdataadmin
Prefix: /tui/iscape/releases

%description
The psdataadmin webapp provides facilities for modifying data in the powersearch database.

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
get /pub/artifacts/psdataadmin/@cruisebuilddate@/powersearchadmin.conf powersearchadmin.conf
get /pub/artifacts/psdataadmin/@cruisebuilddate@/psdataadmin.war psdataadmin.war
bye

svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/publish rpmbuild/publish
svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

cp psdataadmin.war $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
cp powersearchadmin.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf

cp rpmbuild/publish/tomcat.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish
cp rpmbuild/publish/conf/catalina.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/server.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/tomcat-users.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/web.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/iScape/localhost/host-manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/psdataadmin.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/deployscripts/deploypsdataadmin.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/check4redundant_iscape_tags.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT 

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts/psdataadmin.war
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/powersearchadmin.conf
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/catalina.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/server.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/tomcat-users.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/web.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/host-manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/psdataadmin.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
/tui/iscape/releases/is@version@.@release@/deployscripts/deploypsdataadmin.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/check4redundant_iscape_tags.sh

%changelog
* @rpmbuilddate@ Bob Builder <Julia_Dain@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
