#Known issues:
#
#Likey to need :
#tomcat.sh.sprocket
#deploysprocket.sh
#sprocket.xml


#
# AndyChapman30/11/09 This is a lightly modified copy of eBorders-template.spec
# Purpose is to allow a (ANY!) sprocket RPM build to proceed. Thereafter determine what changes are needed
#
#
#
Summary: The TUI sprocket app
Name: sprocketPlus
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/sprocket
Prefix: /tui/iscape/releases

%description
The sprocket apps create the daily updated landing pages, destination content + more for our webapps

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
#get /pub/artifacts/sprocketPlus/@cruisebuilddate@/sprocket.conf sprocket.conf
get /pub/artifacts/sprocketPlus/@cruisebuilddate@/sprocketPlusCruiseTh.war sprocketPlusCruiseTh.war
get /pub/artifacts/sprocketPlus/@cruisebuilddate@/soc-environment.properties soc-environment.properties
bye
# get the publish files from cvs
svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/publish rpmbuild/publish
svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

echo "aaaaa"
pwd
echo "RPM_BUILD_ROOT is $RPM_BUILD_ROOT"
echo "bbbb end"
echo "# Necessary empty file" > sprocketplus.conf
cp sprocketPlusCruiseTh.war $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
cp sprocketplus.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp soc-environment.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape


cp rpmbuild/publish/tomcat.sh.sprocketplus $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
cp rpmbuild/publish/conf/catalina.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/server.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/tomcat-users.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/web.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/iScape/localhost/host-manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost

cp rpmbuild/publish/conf/iScape/localhost/sprocketPlusCruiseTh.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost

cp rpmbuild/publish/logging/classes/log4j.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
cp rpmbuild/publish/logging/lib/commons-logging-1.1.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
cp rpmbuild/publish/logging/lib/log4j-1.2.13.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib

cp rpmbuild/deployscripts/deploysprocketplus.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/check4redundant_iscape_tags.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
chmod +x /tui/iscape/releases/is@version@.@release@/deployscripts/*
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts/sprocketPlusCruiseTh.war
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/sprocketplus.conf
/tui/iscape/releases/is@version@.@release@/rpmiscape/soc-environment.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/catalina.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/server.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/tomcat-users.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/web.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/host-manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/sprocketPlusCruiseTh.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes/log4j.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/commons-logging-1.1.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/log4j-1.2.13.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
/tui/iscape/releases/is@version@.@release@/deployscripts/deploysprocketplus.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/check4redundant_iscape_tags.sh

%changelog
* @rpmbuilddate@ Bob Builder <Julia_Dain@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ /
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
