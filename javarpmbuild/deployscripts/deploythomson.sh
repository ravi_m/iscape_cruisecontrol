#!/bin/sh

# this is the SVN file in iscape_cruisecontrol\trunk\javarpmbuild\deployscripts
# pretty sure this is UNUSED: rpmbuild variant is used instead. ac 7/4/9

#
# Post-install script to deploy the thomson webapp
#   as used by javarpmbuild (i.e. NOT the old rpmbuild)
# Usage: sh deploythomson.sh release_name target_name thomson.properties [logfile]
#
# Examples: deploythomson.sh is01.01.01 iscapet1 thomson-iscapet1.properties powersearch.xsl
#
# Assumes the thomson rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the thomson artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# History follows
#  pre july - not recorded
# 30/07/07 A Chapman     Check for unused substitution values
# 14/09/07 A Chapman     Add template/revision values for this file, to support CMaycock's manifest plan.
# 29/11/07 A Chapman     Conditionally add the i3-precise listener
#

DEPLOYTHOMSON_SH_TEMPLATE=1
deploythomson_sh_cvsrev="$Revision: 1.1 $"
DEPLOYTHOMSON_SH_REVISION=`echo $deploythomson_sh_cvsrev | cut -d" " -f2`

# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`
thishostenv=`echo $thishost | cut -b1-3`
setenvpath="$TUI_ISCAPE/program"
if [ $thishostenv = "ukp" ]
then
  envtype=prod
  setenvpath="$TUI_ISCAPE/config/config/$envtype"
else
  if [ $thishostenv = "ukt" ]
  then
    envtype=pat
    setenvpath="$TUI_ISCAPE/config/config/$envtype"
  else
    echo "unable to identify this host ($thishost) as being either a pat or prod host"
    envtype=not_pat_nor_prod
  fi
fi
#. $TUI_ISCAPE/program/setenv.sh
. $setenvpath/setenv.sh


usage="Usage: $0 release_name target_name thomson_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
THOMSON_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$THOMSON_PROPS" -o ! -r "$THOMSON_PROPS" ]; then
    echo "Couldn't open thomson config file $THOMSON_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi

echo -n "Deploying thomson..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
THOMSON_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $THOMSON_DEPLOY_DIR/conf/thomson.conf $THOMSON_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $THOMSON_DEPLOY_DIR/conf/thomson.conf $THOMSON_PROPS
chmod g+r $THOMSON_DEPLOY_DIR/conf/thomson.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYTHOMSON_SH_TEMPLATE=$DEPLOYTHOMSON_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYTHOMSON_SH_CVSREVISION=$DEPLOYTHOMSON_SH_REVISION" >> $manifestfile
echo "THOMSON-ISCAPE_PROPERTIES_TAGGED_THOMSON_TEMPLATE="`cat $THOMSON_PROPS | grep "^THOMSON_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$envtype" = "not_pat_nor_prod" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $setenvpath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $setenvpath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

# 1st post-deployment step - Conditionally Insert i3-precise listener into server.xml
inject_i3_for_this_instance=true
if [ "$thishost" != "uktwap04" \
  -a "$thishost" != "uktwap05" \
  -a "$thishost" != "uktwap30" \
  -a "$thishost" != "uktwap31" \
  -a "$thishost" != "ukpwap07" \
  -a "$thishost" != "ukpwap06" \
  -a "$thishost" != "ukpwap30" \
  -a "$thishost" != "ukpwap31" ]
then
  inject_i3_for_this_instance=false
fi
this_instance=`echo $TARGET_NAME | sed -e "s/^.*\(.\)$/\1/"`
# Do NOT verify a valid instance here. Buildscripts is not the right place
if [ "$this_instance" != "a" \
  -a "$this_instance" != "b" ]
then
  inject_i3_for_this_instance=false
fi
if [ ! -r /tui/iscape/program/tomcat5.5/apache-tomcat-5.5.12/server/lib/indepthmetric.jar \
  -a ! -r /tui/iscape/program/tomcat5.0/jakarta-tomcat-5.0.28/server/lib/indepthmetric.jar ]
then
  inject_i3_for_this_instance=false
fi

if [ "$inject_i3_for_this_instance" = "true" ]
then 
  echo "i3 JMX Metrics monitoring is being ENABLED"
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 1 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->\)~\1<Listener className=\"com.precise.javaperf.extensions.tomcat.JMXMetricsLoaderListener\"/>~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
else
  echo "i3 JMX Metrics monitoring is not required for this instance"
fi


echo "Done." | tee -a $LOGFILE
