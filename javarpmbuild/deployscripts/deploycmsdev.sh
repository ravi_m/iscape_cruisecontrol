#!/bin/bash
#
# Post-install script to deploy cmsdev 
#   as used by javarpmbuild (i.e. NOT the old rpmbuild)
# Usage: deploycmsdev.sh release_name target_name [logfile_path]
#
# Examples: deploycmsdev.sh is01.00.01 iscapet1 

# set up the environment
TUI_ISCAPE=/tui/iscape
. $TUI_ISCAPE/program/setenv.sh
export JAVA_HOME=$JAVA14

usage="Usage: $0 release_name target_name [logfile_path]"

# New release dir is specified as first arg, target deployment directory as second
RELEASE_NAME=$1
TARGET_NAME=$2

if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage
    exit
fi

# Optional third argument is log file. If not specified
# then output goes to stdout
if [ $# = 3 ]; then
    LOGFILE="$3"
fi

RELEASE_DIR=$TUI_ISCAPE/releases/$RELEASE_NAME
cd $RELEASE_DIR

if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 

echo -n "Unzipping cmsdev..."
unzip ../cmsdev/cmsdev.zip | tee -a $LOGFILE >/dev/null

chmod -R g+rw .

echo "Done." | tee -a $LOGFILE
