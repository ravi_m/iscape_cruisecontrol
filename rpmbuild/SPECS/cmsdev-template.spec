Summary: The cmsdev component for thomson on iScape
Name: cmsdev
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/thomson
Prefix: /tui/iscape/releases

%description
cmsdev provides content and configuration data for iScape applications.

%prep
# get the binary artifacts from the build machine
ftp -i -v -n  10.40.4.172 << bye
user "anonymous" "bob"
binary
get /pub/artifacts/cmsdev/@cruisebuilddate@/cmsdev.zip cmsdev.zip
bye
# get the post-install scripts 
cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/iscape_cruisecontrol checkout rpmbuild/deployscripts
# copy cmsdev.zip to ftp area for non-rpm use on web servers
cp cmsdev.zip /srv/ftp/pub/rpms/cmsdev-@version@-@release@.zip

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/cmsdev
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp cmsdev.zip $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/cmsdev
cp rpmbuild/deployscripts/deploycmsdev.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/cmsdev/cmsdev.zip
/tui/iscape/releases/is@version@.@release@/deployscripts/deploycmsdev.sh

%changelog
* @rpmbuilddate@ Bob Builder <Julia_Dain@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
