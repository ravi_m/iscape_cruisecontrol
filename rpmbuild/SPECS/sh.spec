Summary: A virtual package for /bin/sh
Name: sh
Version: 01.01
Release: 01
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/thomson
Prefix: /tui/iscape/current
Provides: /bin/sh

%description
A virtual package for /bin/sh

%prep

%build

%install

%clean

%files


%changelog
* Tue Sep 26 2006 Julia Dain <Julia_Dain@tui-uk.co.uk> 
- first draft 
