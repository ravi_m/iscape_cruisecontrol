#!/bin/sh

############################################################################
#
# deployfcao.sh
#
# Post-install script to deploy the fcao webapp
# Usage: sh deployfcao.sh release_name target_name fcao.properties [logfile]
#
# Examples: deployfcao.sh is01.01.01 iscapet1 fcao-iscapet1.properties powersearch.xsl
#
# Assumes the fcao rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the fcao artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for wish deployments (OLD rpmbuild), and likely for fcao (newrpmbuild) deployments too. 
#  Is also the default deploy<brand>.sh used by Richard Hands when creating an RPM solution for a new brand.
#
# History follows
#  pre july - not recorded
# 30/07/07 A Chapman     Check for unused substitution values
# 14/09/07 A Chapman     Add template/revision values for this file, to support CMaycock's manifest plan.
# 29/11/07 A Chapman     Conditionally add the i3-precise listener
# 09/04/07 A Chapman     wish_pat 2nd stream becomes 1st stream, so gets i3
# 08/08/08 A Chapman     Changed to support WISH_TEMPLATE_VERSION
# 14/11/08 A Chapman     Update to standard environment identification used for all recent brands
# 18/12/08 A Chapman     Deploy a cvs *datacash.conf if it exists (initially needed for wish : dec08)
#                         support both initial and eventual naming
# 13/02/09 A Chapman     Add the standard apply_dos2unix(), such that it appears in all new brands
# 16/02/09 A Chapman     Remove dos2unix, as now done by check4redundant_iscape_tags.sh
# 02/11/10 Ramesh Babu   Removed PAT servers list from Precise servers list section as per discussion with Chris on 2/11/2010.
# 14/01/11 Ramesh Babu   Removed ukpwap07 and ukpwap60 servers from precise list as per Chris mail - Fri 14/01/2011 11:14
#
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################


DEPLOYFCAO_SH_TEMPLATE=1
deployfcao_sh_cvsrev="$Revision$"
DEPLOYFCAO_SH_REVISION=`echo $deployfcao_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name fcao_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
FCAO_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$FCAO_PROPS" -o ! -r "$FCAO_PROPS" ]; then
    echo "Couldn't open fcao config file $FCAO_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/$LOCAL_ISCAPE_ENVIRONMENT"




echo -n "Deploying fcao..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
FCAO_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $FCAO_DEPLOY_DIR/conf/accom.conf $FCAO_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $FCAO_DEPLOY_DIR/conf/accom.conf $FCAO_PROPS
chmod g+r $FCAO_DEPLOY_DIR/conf/accom.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYFCAO_SH_TEMPLATE=$DEPLOYFCAO_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYFCAO_SH_CVSREVISION=$DEPLOYFCAO_SH_REVISION" >> $manifestfile
echo "FCAO-ISCAPE_PROPERTIES_TAGGED_FCAO_TEMPLATE="`cat $FCAO_PROPS | egrep "^FCAO_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

# 1st post-deployment step - Conditionally Insert i3-precise listener into server.xml
inject_i3_for_this_instance=true
if [ "$thishost" != "ukpwap06" \
  -a "$thishost" != "ukpwap30" \
  -a "$thishost" != "ukpwap31" \
  -a "$thishost" != "ukpwap61" \
  ]
then
  inject_i3_for_this_instance=false
fi
this_instance=`echo $TARGET_NAME | sed -e "s/^.*\(.\)$/\1/"`
# Do NOT verify a valid instance here. Buildscripts is not the right place
if [ "$this_instance" != "a" \
  -a "$this_instance" != "b" ]
then
  inject_i3_for_this_instance=false
fi
if [ ! -r /tui/iscape/program/tomcat5.5/apache-tomcat-5.5.12/server/lib/indepthmetric.jar ]
then
  inject_i3_for_this_instance=false
fi

if [ "$inject_i3_for_this_instance" = "true" ]
then 
  echo "i3 JMX Metrics monitoring is being ENABLED" | tee -a $LOGFILE
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 1 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->\)~\1<Listener className=\"com.precise.javaperf.extensions.tomcat.JMXMetricsLoaderListener\"/>~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
else
  echo "i3 JMX Metrics monitoring is not required for this instance" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
