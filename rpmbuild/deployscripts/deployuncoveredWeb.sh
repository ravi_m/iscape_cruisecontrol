#!/bin/sh

############################################################################
#
# deployuncoveredWeb.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the uncoveredWeb webapp
# Usage: sh deployuncoveredWeb.sh release_name target_name uncoveredWeb.properties [logfile]
#
# Examples: deployuncoveredWeb.sh uncoveredWeb01.00.00 iscapet1 uncoveredWeb-iscapet1.properties 
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for uncoveredWeb deployments. 
#
# History follows
# 14/05/10 Ramesh Babu   Created the file to test Holidays Uncovered application.
# 17/05/10 Ramesh Babu   Modified the file to test the test tag builds.
# 09/07/10 Ramesh Babu   Creating the softlink for subject-images under webapps as per Chris Maycock mail - Thu 08/07/2010 08:37
# 21/09/10 Ramesh Babu   Included post deployment step to enable Tomcat jobs on UKPWAP76 server.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
#
############################################################################


DEPLOYUNCOVEREDWEB_SH_TEMPLATE=1
deployuncoveredWeb_sh_cvsrev="$Revision$"
DEPLOYUNCOVEREDWEB_SH_REVISION=`echo $deployuncoveredWeb_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name uncoveredWeb _config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
UNCOVEREDWEB_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$UNCOVEREDWEB_PROPS" -o ! -r "$UNCOVEREDWEB_PROPS" ]; then
    echo "Couldn't open UncoveredWeb config file $UNCOVEREDWEB_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"
# =================================================================
# THIS SECTION IS TEMPORARY STILL THE IMAGES ISSUE RESOLVES
# =================================================================
INITIAL_DIRECTORY=`pwd`

echo -n "Deploying UncoveredWeb ..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
UNCOVEREDWEB_DEPLOY_DIR="`pwd`/publish"
# =================================================================
# THIS SECTION IS TEMPORARY STILL THE IMAGES ISSUE RESOLVES
# =================================================================
UNCOVEREDWEB_WEBAPPS_DIR="`pwd`/publish/webapps"
# =================================================================
# THIS SECTION IS REQUIERD TO ENABLE TOMCAT JOBS IN UKPWAP76 SERVER
# =================================================================
UNCOVEREDWEB_CONF_DIR="`pwd`/publish/conf"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $UNCOVEREDWEB_DEPLOY_DIR/conf/uncoveredWeb.conf $UNCOVEREDWEB_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $UNCOVEREDWEB_DEPLOY_DIR/conf/uncoveredWeb.conf $UNCOVEREDWEB_PROPS
chmod g+r $UNCOVEREDWEB_DEPLOY_DIR/conf/uncoveredWeb.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYUNCOVEREDWEB_SH_TEMPLATE=$DEPLOYUNCOVEREDWEB_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYUNCOVEREDWEB_SH_CVSREVISION=$DEPLOYUNCOVEREDWEB_SH_REVISION" >> $manifestfile
echo "UNCOVEREDWEB-ISCAPE_PROPERTIES_TAGGED_UNCOVEREDWEB_TEMPLATE="`cat $UNCOVEREDWEB_PROPS | grep "^UNCOVEREDWEB_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp

# =================================================================
# THIS SECTION IS TEMPORARY STILL THE IMAGES ISSUE RESOLVES
# =================================================================
# Create subject-images soft link in webapps folder
echo "Post deployment step1..." | tee -a $LOGFILE
echo "Creating subject-images softlink under webapps folder..." | tee -a $LOGFILE
cd $UNCOVEREDWEB_WEBAPPS_DIR
ln -s /tui/iscape/hu-app/htdocs/subject-images subject-images

cd $INITIAL_DIRECTORY

# ========================== END ==================================


# =================================================================
# THIS SECTION IS REQUIERD TO ENABLE TOMCAT JOBS IN UKPWAP76 SERVER
# =================================================================
echo "Post deployment step2.." | tee -a $LOGFILE
if [ "$thishost" = "ukpwap76.tui.de" ]
then
	echo "Updating the conf file to enable Tomcat jobs in UKPWAP76..." | tee -a $LOGFILE
	cd $UNCOVEREDWEB_CONF_DIR
	sed -e 's/=false/=true/g' -e 's/hib.show_sql=true/hib.show_sql=false/g' < uncoveredWeb.conf > uncoveredWeb.conf.new
	mv uncoveredWeb.conf uncoveredWeb.conf.orig
	mv uncoveredWeb.conf.new uncoveredWeb.conf
	cd $INITIAL_DIRECTORY
else
	echo "This step is not required for this server $thishost...." | tee -a $LOGFILE
fi

echo "Deployment performed. Post-Deployment Step(s) completed." | tee -a $LOGFILE

echo "Done." | tee -a $LOGFILE
