#!/bin/sh

############################################################################
#
# set_distributedcache_ports.sh
#   as used by deploywish.sh, and soon, all brands (as brands start using Distributed Cache (if it provides significant benefit))
#
# Post-Deploy script to insert the appropriate Distributed Cache listen port numbers
#
# Invokation. Not intended for interactive use. Invoked by deploy<BRAND>.sh
#
# Parameters:
# 1) the deployed BRAND.conf filespec
# 2) the brand seed number (see below)
# 3) the instance letter (lowercase)
#
# brand seed number for each brand is defined in port_numbering_plan_#4f_Apr09.xls or later. 
# which can be accessed from CVS hannover_scripts\documentation_and_support_tools\docs\port_numbering_plan*.xls
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# History follows
# 07/04/09 A Chapman     Created for Wish brand. Will support All brands.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################


INSTANCE_CONF=$1
SEED=$2
INSTANCE_LETTER=$3


fatal()
{
  echo " "
  echo "FATAL: $1"
  echo "$2"
  echo " "
  exit 1
}

debug()
{
  echo "DEBUG $1 "
}









# Main script starts here ....................................................................

tmpconf=${INSTANCE_CONF}_preCachePorts
listenport=`expr $SEED - 1`
alphabet="a b c d e f g h i j k l m n o p q r s t u v w x y z 9"
for thisinstance in $alphabet
do
  listenport=`expr $listenport + 1`
  if [ "$thisinstance" = "$INSTANCE_LETTER" ]
  then
    break
  fi
  test "$thisinstance" = "9" && fatal "Passed invalid instance = $INSTANCE_LETTER"
done






# Create new conf file: Note: cp not mv, to preserve permissions
cp $INSTANCE_CONF $tmpconf
cc="Cache.Clustering"
cat $tmpconf | sed -e "s/\($cc.HOST_TCP_PORT=\).*/\1$listenport/" > $INSTANCE_CONF
# rm $tmpconf

echo "   Instance $INSTANCE_LETTER: Replaced $cc.HOST_TCP_PORT value with port $listenport"

