#!/bin/sh

############################################################################
#
# deploygroundtrader.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# To be invoked by iscapectl2
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# History follows
# 28/11/08 A Chapman   Created
# 13&19/01/09 AChapman ReCreated to new spec
# 20/01/09 A Chapman   Bugfixes: a)datcash typo; b)dos2unix
# 25/02/09 A Chapman   Bugfix + deploy datacash.conf from cvs
# 02/03/09 A Chapman   Bugfix to Bugfix! (datacash.HostName)
# 06/03/09 A Chapman   bin/gt-cps-batch.sh needs execute permision
# 13/03/09 A Chapman   Bugfix - missed the 'misc' directory from the routing file filespec
# 19/03/09 A Chapman   Tidy & support new LogWriter items
# 14/03/11 Ramesh Babu Updated the script to support gt02.00.xx release conf changes.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################


DEPLOYGROUNDTRADER_SH_TEMPLATE=1
deploygroundtrader_sh_cvsrev="$Revision$"
DEPLOYGROUNDTRADER_SH_REVISION=`echo $deploygroundtrader_sh_cvsrev | cut -d" " -f2`

# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name groundtrader_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
GT_PROPS=$3


declare_error()
{
  echo " "
  echo "***********************************************************************"
  echo $1
  echo "***********************************************************************"
  echo " "
}



subst_vars()
{
  TGT_FILE=$1
  tgtabs=$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/$gt_subdir/$TGT_FILE

  get_only_cct_filename

  mv ${tgtabs} ${tgtabs}_presubst
  # Protect against the 'final line has no LF scenario'
  echo " " >> ${tgtabs}_presubst
  cat ${tgtabs}_presubst \
   | sed -e "s!XXXXX(1)!tui/iscape/releases/$RELEASE_NAME/$TARGET_NAME!g" \
   | sed -e "s!XXXXX(2)!$gt_subdir!g" \
   | sed -e "s!XXXXX(3)!$cct_expire_days!g" \
   | sed -e "s!YY-MM-DD!$gt_cert_date!g" \
   | sed -e "s!gt.keystoreFileName=.*!gt.keystoreFileName=$cctfilename!g" \
   | sed -e "s!gt.GT-GBP.ORACLE.URL=.*!gt.GT-GBP.ORACLE.URL=$GT_GBP_ORACLE_URL!g" \
   | sed -e "s!gt.GT-GBP.ORACLE.UserId=.*!gt.GT-GBP.ORACLE.UserId=$GT_GBP_ORACLE_UserId!g" \
   | sed -e "s!gt.GT-GBP.ORACLE.Password=.*!gt.GT-GBP.ORACLE.Password=$GT_GBP_ORACLE_Password!g" \
   | sed -e "s!gt.GT-EUR.ORACLE.URL=.*!gt.GT-EUR.ORACLE.URL=$GT_EUR_ORACLE_URL!g" \
   | sed -e "s!gt.GT-EUR.ORACLE.UserId=.*!gt.GT-EUR.ORACLE.UserId=$GT_EUR_ORACLE_UserId!g" \
   | sed -e "s!gt.GT-EUR.ORACLE.Password=.*!gt.GT-EUR.ORACLE.Password=$GT_EUR_ORACLE_Password!g" \
   | sed -e "s!gt.SELECT-QUERY=.*!gt.SELECT-QUERY=$SELECT_QUERY!g" \
   | sed -e "s!gt.SELECT-QUERY-COUNT=.*!gt.SELECT-QUERY-COUNT=$SELECT_QUERY_COUNT!g" \
   | sed -e "s!gt.CREATEPAYMENT-PROCEDURE=.*!gt.CREATEPAYMENT-PROCEDURE=$CREATEPAYMENT_PROCEDURE!g" \
   | sed -e "s!gt.PROCESSEDBYCPS-PROCEDURE=.*!gt.PROCESSEDBYCPS-PROCEDURE=$PROCESSEDBYCPS_PROCEDURE!g" \
   | sed -e "s!gt.PROCESSEDBYDC-PROCEDURE=.*!gt.PROCESSEDBYDC-PROCEDURE=$PROCESSEDBYDC_PROCEDURE!g" \
   | sed -e "s!gt.DELETETRACK2-PROCEDURE=.*!gt.DELETETRACK2-PROCEDURE=$DELETETRACK2_PROCEDURE!g" \
   | sed -e "s�gt.datacash.HostName=.*�gt.datacash.HostName=$datacash_HostName�g" \
   | sed -e "s!JAVA_HOME=.*!JAVA_HOME=$JAVA_HOME!g" \
   | sed -e "s!gt.com.tui.uk.log.LogWriter.LogLevel=.*!gt.com.tui.uk.log.LogWriter.LogLevel=$gt_logging_LogLevel!g" \
   | sed -e "s!gt.com.tui.uk.log.LogWriter.consoleout=.*!gt.com.tui.uk.log.LogWriter.consoleout=$gt_logging_consoleout!g" \
   | sed -e "s!gt.com.tui.uk.log.LogWriter.fileout=.*!gt.com.tui.uk.log.LogWriter.fileout=$gt_logging_fileout!g" \
   | sed -e "s!gt.com.tui.uk.log.LogWriter.asyncLogging=.*!gt.com.tui.uk.log.LogWriter.asyncLogging=$gt_logging_asyncLogging!g" \
   | sed -e "s!gt.com.tui.uk.log.LogWriter.maxFileSize=.*!gt.com.tui.uk.log.LogWriter.maxFileSize=$gt_logging_maxFileSize!g" \
   | sed -e "s!gt.com.tui.uk.log.LogWriter.maxBackupSize=.*!gt.com.tui.uk.log.LogWriter.maxBackupSize=$gt_logging_maxBackupSize!g" \
   | sed -e "s!gt.com.tui.uk.log.LogWriter.datePattern=.*!gt.com.tui.uk.log.LogWriter.datePattern=$gt_logging_datePattern!g" \
   | sed -e "s!gt.GT-GBP.vtid=.*!gt.GT-GBP.vtid=$GT_GBP_vtid!g" \
   | sed -e "s!gt.GT-GBP.tid=.*!gt.GT-GBP.tid=$GT_GBP_tid!g" \
   | sed -e "s!gt.GT-EUR.vtid=.*!gt.GT-EUR.vtid=$GT_EUR_vtid!g" \
   | sed -e "s!gt.GT-EUR.tid=.*!gt.GT-EUR.tid=$GT_EUR_tid!g" \
   | sed -e "s!gt.fraudcheckenabled=.*!gt.fraudcheckenabled=$FRAUDCHECKENABLED!g" \
   | sed -e "s!gt.totalCountOfCertificates=.*!gt.totalCountOfCertificates=$TOTALCOUNT!g" \
   | sed -e "s!gt.certificates=.*!gt.certificates=$GT_CERTIFICATES!g" \
   | sed -e "s!gt.cert1.keystoreType=.*!gt.cert1.keystoreType=$cert1_keystoreType!g" \
   | sed -e "s!gt.cert1.keystorePassword=.*!gt.cert1.keystorePassword=$cert1_keystorePassword!g" \
   | sed -e "s!gt.cert2.keystoreType=.*!gt.cert2.keystoreType=$cert2_keystoreType!g" \
   | sed -e "s!gt.cert2.keystorePassword=.*!gt.cert2.keystorePassword=$cert2_keystorePassword!g" \
   | sed -e "s!gt.cert1.algorithm=.*!gt.cert1.algorithm=$cert1_algorithm!g" \
   | sed -e "s!gt.cert2.algorithm=.*!gt.cert2.algorithm=$cert2_algorithm!g" \
   | sed -e "s!gt.RECORDNOTES-PROCEDURE=.*!gt.RECORDNOTES-PROCEDURE=$RECORDNOTES_PROCEDURE!g" \
   > ${tgtabs}
   # rm ${tgtabs}_presubst

# 02/03/09 Bugfix above. gt.datacash.HostName was problematic. Last week found that replaceing " with " worked.
#  now now longer works. Replaced ! with alternative character (� should be safe) & it works again
}

#=======================================================================
# Removed the below lines from above function.
#=======================================================================
#RB#   | sed -e "s!gt.keystoreType=.*!gt.keystoreType=$keystoreType!g" \
#RB#   | sed -e "s!gt.keystorePassword=.*!gt.keystorePassword=$keystorePassword!g" \
#RB#   | sed -e "s!gt.algorithm=.*!gt.algorithm=$algorithm!g" \

get_only_cct_filename()
{
  filenames=`ls $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/$gt_subdir/conf/certificate/tui-cluetrader-????-??-??.cer`
  cctcount=0
  for thisfile in $filenames
  do
    cctfilename=$thisfile
    cctcount=`expr $cctcount + 1`
  done

  if [ ! $cctcount = 1 ]
  then
    echo "ERROR: Should be one, and only one, certificate matching below filespec (delivered from development via gt-cps-batch.zip)"
    echo "$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/$gt_subdir/conf/certificate/tui-cluetrader-????-??-??.cer"
    exit 1
  fi
  #echo "determined unique cctname is $cctfilename"
}


apply_dos2unix()
{
  # removes any ^M line endings from the specified file. 
  # only parameter specifies file to clean. Can be absolute/relative/no path.
  # Beware: dos2unix behaves differently on solaris & unix. Failsafe.
  echo " Performing dos2unix on file: $1"
  ostype=`uname`
  if [ "$ostype" = "SunOS" ]
  then
    mv $1 ${1}_preTounix
    dos2unix ${1}_preTounix > $1 2>/dev/null
  else
    dos2unix $1 > /dev/null 2>/dev/null
  fi
}






# Main script starts here ..................................................

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage
    exit 1
fi
if [ ! -f "$GT_PROPS" -o ! -r "$GT_PROPS" ]; then
    echo "Couldn't open groundtrader config file $GT_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"
JAVA_HOME=$JAVA15 export JAVA_HOME


echo -n "Deploying groundtrader..."  | tee -a $LOGFILE


# Quick&Dirty: Read environment-specific groundtrader config values from cvs file
gt_subdir=`cat $GT_PROPS | grep "CPS.GROUNDTRADER.gt_subdir" | cut -d"=" -f2`
cct_expire_days=`cat $GT_PROPS | grep "CPS.GROUNDTRADER.cct_expire_days" | cut -d"=" -f2`
gt_cert_date=`date '+%y-%m-%d'`

GT_GBP_ORACLE_URL=`cat $GT_PROPS | grep "gt.GT-GBP.ORACLE.URL" | cut -d"=" -f2-`
GT_GBP_ORACLE_UserId=`cat $GT_PROPS | grep "gt.GT-GBP.ORACLE.UserId" | cut -d"=" -f2-`
GT_GBP_ORACLE_Password=`cat $GT_PROPS | grep "gt.GT-GBP.ORACLE.Password" | cut -d"=" -f2-`
GT_EUR_ORACLE_URL=`cat $GT_PROPS | grep "gt.GT-EUR.ORACLE.URL" | cut -d"=" -f2-`
GT_EUR_ORACLE_UserId=`cat $GT_PROPS | grep "gt.GT-EUR.ORACLE.UserId" | cut -d"=" -f2-`
GT_EUR_ORACLE_Password=`cat $GT_PROPS | grep "gt.GT-EUR.ORACLE.Password" | cut -d"=" -f2-`
SELECT_QUERY=`cat $GT_PROPS | grep "gt.SELECT-QUERY=" | cut -d"=" -f2-`
SELECT_QUERY_COUNT=`cat $GT_PROPS | grep "gt.SELECT-QUERY-COUNT" | cut -d"=" -f2-`
CREATEPAYMENT_PROCEDURE=`cat $GT_PROPS | grep "gt.CREATEPAYMENT-PROCEDURE" | cut -d"=" -f2-`
PROCESSEDBYCPS_PROCEDURE=`cat $GT_PROPS | grep "gt.PROCESSEDBYCPS-PROCEDURE" | cut -d"=" -f2-`
PROCESSEDBYDC_PROCEDURE=`cat $GT_PROPS | grep "gt.PROCESSEDBYDC-PROCEDURE" | cut -d"=" -f2-`
DELETETRACK2_PROCEDURE=`cat $GT_PROPS | grep "gt.DELETETRACK2-PROCEDURE" | cut -d"=" -f2-`
RECORDNOTES_PROCEDURE=`cat $GT_PROPS | grep "gt.RECORDNOTES-PROCEDURE" | cut -d"=" -f2-`
datacash_HostName=`cat $GT_PROPS | grep "gt.datacash.HostName" | cut -d"=" -f2-`

GT_GBP_vtid=`cat $GT_PROPS | grep "gt.GT-GBP.vtid" | cut -d"=" -f2-`
GT_EUR_vtid=`cat $GT_PROPS | grep "gt.GT-EUR.vtid" | cut -d"=" -f2-`

#RB#keystoreType=`cat $GT_PROPS | grep "gt.keystoreType" | cut -d"=" -f2-`
#RB#keystorePassword=`cat $GT_PROPS | grep "gt.keystorePassword" | cut -d"=" -f2-`
#RB#algorithm=`cat $GT_PROPS | grep "gt.algorithm" | cut -d"=" -f2-`

cert1_keystoreType=`cat $GT_PROPS | grep "gt.cert1.keystoreType" | cut -d"=" -f2-`
cert1_keystorePassword=`cat $GT_PROPS | grep "gt.cert1.keystorePassword" | cut -d"=" -f2-`
cert1_algorithm=`cat $GT_PROPS | grep "gt.cert1.algorithm" | cut -d"=" -f2-`
cert2_keystoreType=`cat $GT_PROPS | grep "gt.cert2.keystoreType" | cut -d"=" -f2-`
cert2_keystorePassword=`cat $GT_PROPS | grep "gt.cert2.keystorePassword" | cut -d"=" -f2-`
cert2_algorithm=`cat $GT_PROPS | grep "gt.cert2.algorithm" | cut -d"=" -f2-`

FRAUDCHECKENABLED=`cat $GT_PROPS | grep "gt.fraudcheckenabled" | cut -d"=" -f2-`
TOTALCOUNT=`cat $GT_PROPS | grep "gt.totalCountOfCertificates" | cut -d"=" -f2-`
GT_CERTIFICATES=`cat $GT_PROPS | grep "gt.certificates" | cut -d"=" -f2-`

#RB#gt_logging_LogLevel=`cat $GT_PROPS | grep "gt.com.tui.uk.log.LogWriter.LogLevel" | cut -d"=" -f2-`
#RB#gt_logging_consoleout=`cat $GT_PROPS | grep "gt.com.tui.uk.log.LogWriter.consoleout" | cut -d"=" -f2-`
#RB#gt_logging_fileout=`cat $GT_PROPS | grep "gt.com.tui.uk.log.LogWriter.fileout" | cut -d"=" -f2-`
#RB#gt_logging_asyncLogging=`cat $GT_PROPS | grep "gt.com.tui.uk.log.LogWriter.asyncLogging" | cut -d"=" -f2-`
#RB#gt_logging_maxFileSize=`cat $GT_PROPS | grep "gt.com.tui.uk.log.LogWriter.maxFileSize" | cut -d"=" -f2-`
#RB#gt_logging_maxBackupSize=`cat $GT_PROPS | grep "gt.com.tui.uk.log.LogWriter.maxBackupSize" | cut -d"=" -f2-`
#RB#gt_logging_datePattern=`cat $GT_PROPS | grep "gt.com.tui.uk.log.LogWriter.datePattern" | cut -d"=" -f2-`




echo " About to create : $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/$gt_subdir "
mkdir -p $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/$gt_subdir
cd  $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/$gt_subdir

# Create directory structure
mkdir -p logs
mkdir -p GT-GBP
mkdir -p GT-EUR
mkdir -p certificate-expiry

# Unzip the zipfile
unzip $TUI_ISCAPE/releases/$RELEASE_NAME/groundtrader/gt-cps-batch.zip

# Now dos2unix to sanitise key files
apply_dos2unix conf/gt.conf
apply_dos2unix conf/datacash.conf
apply_dos2unix bin/gt-cps-batch.ini


# Now substitute environment-specific values as appropriate
echo " Substituting GroundTrader environment-specific values in conf/gt.conf"
subst_vars conf/gt.conf
echo " Substituting GroundTrader environment-specific values in bin/gt-cps-batch.ini"
subst_vars bin/gt-cps-batch.ini




echo "Checking for unused substitution values in the java conf file"
GROUNDTRADER_DEPLOY_DIR="$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/$gt_subdir"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $GROUNDTRADER_DEPLOY_DIR/conf/gt.conf $GT_PROPS


# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYGROUNDTRADER_SH_TEMPLATE=$DEPLOYGROUNDTRADER_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYGROUNDTRADER_SH_CVSREVISION=$DEPLOYGROUNDTRADER_SH_REVISION" >> $manifestfile
echo "GROUNDTRADER-ISCAPE_PROPERTIES_TAGGED_GROUNDTRADER_TEMPLATE="`cat $GT_PROPS | grep "^GROUNDTRADER_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp



# Post=Deployment steps will start here if any required.
echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE


# 1st post-deployment step - Replace the datacash.conf with file from cvs
dcfilename="datacash.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/gt/conf/$dcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/gt/conf/$dcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/gt/conf/${dcfilename}_fromdev
else
  echo "WARNING: No $dcfilename provided from development. Will attempt to deploy environment-specific file anyway"
fi

dcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/groundtrader/$dcfilename
if [ -f $dcconf ]
then
  cp $dcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/gt/conf
  if [ "$?" = "0" ]
  then
    echo "Substitution of $dcfilename completed successfully"
  else
    declare_error "ERROR: Substitution of $dcfilename failed, whilst copying from $dcconf"
  fi
else
  declare_error "ERROR: Deployment is lacking $dcfilename, as was unable to find environment-specific variant at location: $dcconf"
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE



# 2nd post-deployment step - bin/gt-cps-batch.sh needs execute permision
chmod ug+x $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/gt/bin/gt-cps-batch.sh
chmod ug+x $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/gt/bin/gt-cps-batch.ini
echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE



echo "Done." | tee -a $LOGFILE




