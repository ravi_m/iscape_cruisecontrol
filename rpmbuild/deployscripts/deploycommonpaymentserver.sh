#!/bin/sh

############################################################################
#
# deploycommonpaymentserver.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the cps webapp
# Usage: sh deploycps.sh release_name target_name cps.properties [logfile]
#
# Examples: deploycps.sh is01.01.01 iscapet1 cps-iscapet1.properties powersearch.xsl
#
# Assumes the cps rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the cps artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for commonpaymentserver deployments
#
# Notes:
# - i3/precise not yet added. Add hostnames
#
#
# History follows
# 30/09/08 R Hands     created as copy from thomson script
# 30/09/08 A Chapman   some customisation for CPS. Mostly update to fcsun approach for ID hosttype
# 02/10/08 A Chapman   Add post-deployment steps to customise the server.xml for CPS
# 06/10/08 A Chapman   Amend iScapelocalhost/cps.xml with unique ID, NOT server.xml
# 07/11/08 A Chapman   Change unique CPS key from pay_[abc] to cps[tp]8[89][abc]
# 27/11/08 A Chapman   Support GroundTrader
# 09/12/08 A Chapman   Support new CPS datacash.conf file
# 04/02/09 A Chapman   Remove redundant Groundtrader PostDeploy step (GT is now a separate deployment - standalone)
# 05/05/10 Ramesh Babu Support for new conf files cv2avs.conf, threedsecurity.conf and promodiscservice.conf like datcash.conf file.
#			     This change is required from cps12.xx.xx releases - mail reference Wed 05/05/2010 09:57.
# 02/11/10 Ramesh Babu   Removed PAT servers list from Precise servers list section as per discussion with Chris on 2/11/2010.
# 16/05/11 Ramesh Babu Support for new conf file carddetails.conf like datacash.conf file. It is required from CPS16.xx.xx release.
# 17/06/14 Nitin Das   Support for new conf file countrylist.conf.
#
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################

DEPLOYCPS_SH_TEMPLATE=1
deploycps_sh_svnrev="$Revision$"
DEPLOYCPS_SH_REVISION=`echo $deploycps_sh_svnrev | cut -d" " -f2`


declare_error()
{
  echo " "
  echo "***********************************************************************"
  echo $1
  echo "***********************************************************************"
  echo " "
}



# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`
thishostenv=`echo $thishost | cut -b1-3`



usage="Usage: $0 release_name target_name cps_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
CPS_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$CPS_PROPS" -o ! -r "$CPS_PROPS" ]; then
    echo "Couldn't open cps config file $CPS_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"



echo -n "Deploying cps..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
CPS_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $CPS_DEPLOY_DIR/conf/cps.conf $CPS_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $CPS_DEPLOY_DIR/conf/cps.conf $CPS_PROPS
chmod g+r $CPS_DEPLOY_DIR/conf/cps.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYCPS_SH_TEMPLATE=$DEPLOYCPS_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYCPS_SH_CVSREVISION=$DEPLOYCPS_SH_REVISION" >> $manifestfile
echo "CPS-ISCAPE_PROPERTIES_TAGGED_CPS_TEMPLATE="`cat $CPS_PROPS | grep "^CPS_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp

echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE





# 1st post-deployment step - Conditionally Insert i3-precise listener into server.xml
inject_i3_for_this_instance=true
if [ "$thishost" != "ukpwap07" \
  -a "$thishost" != "ukpwap06" \
  -a "$thishost" != "ukpwap30" \
  -a "$thishost" != "ukpwap31" ]
then
  inject_i3_for_this_instance=false
fi
this_instance=`echo $TARGET_NAME | sed -e "s/^.*\(.\)$/\1/"`
# Do NOT verify a valid instance here. Buildscripts is not the right place
if [ "$this_instance" != "a" \
  -a "$this_instance" != "b" ]
then
  inject_i3_for_this_instance=false
fi
if [ ! -r /tui/iscape/program/tomcat5.5/apache-tomcat-5.5.12/server/lib/indepthmetric.jar \
  -a ! -r /tui/iscape/program/tomcat5.0/jakarta-tomcat-5.0.28/server/lib/indepthmetric.jar ]
then
  inject_i3_for_this_instance=false
fi

if [ "$inject_i3_for_this_instance" = "true" ]
then 
  echo "i3 JMX Metrics monitoring is being ENABLED" | tee -a $LOGFILE
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 1 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->\)~\1<Listener className=\"com.precise.javaperf.extensions.tomcat.JMXMetricsLoaderListener\"/>~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
else
  echo "i3 JMX Metrics monitoring is not required for this instance" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE


# 2nd post-deployment step - server.xml : the http Connector becomes the xmlrpc Connector
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e "s~{tomcat.httpport}~{tomcat.xmlrpcport}~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
    rm $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE


# 3rd post-deployment step - inject unique ID code into iScape/localhost/cps.xml.
# iscape_hostnumber & thisoneinstancenumber is inherited from iscapectl2.ksh
    iscape_hosttype=`hostname | cut -b3`
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/cps.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/cps.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/cps.xml_org | \
        sed -e "s/replace_this_with_unique_id/cps${iscape_hosttype}${iscape_hostnumber}$thisoneinstancenumber/" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/cps.xml
    rm $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/cps.xml_org
echo "Post-Deployment Step 3 complete." | tee -a $LOGFILE


# 4th post-deployment step - Replace the datacash.conf with value from cvs
dcfilename="datacash.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$dcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$dcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${dcfilename}_fromdev
  dcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$dcfilename
  if [ -f $dcconf ]
  then
    cp $dcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $dcfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $dcfilename failed, whilst copying from $dcconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $dcfilename, as was unable to find environment-specific variant at location: $dcconf"
  fi
else
  echo "WARNING: No $dcfilename provided from development. Assuming this is rebuild of old CPS (pre datacash.conf)"
fi
echo "Post-Deployment Step 4 complete." | tee -a $LOGFILE

# 5th post-deployment step - Replace the cv2avs.conf with value from cvs
cv2filename="cv2avs.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$cv2filename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$cv2filename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${cv2filename}_fromdev
  cv2conf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$cv2filename
  if [ -f $cv2conf ]
  then
    cp $cv2conf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $cv2filename completed successfully"
    else
      declare_error "ERROR: Substitution of $cv2filename failed, whilst copying from $cv2conf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $cv2filename, as was unable to find environment-specific variant at location: $cv2conf"
  fi
else
  echo "WARNING: No $cv2filename provided from development. Assuming this is rebuild of old CPS (pre cv2avs.conf)"
fi
echo "Post-Deployment Step 5 complete." | tee -a $LOGFILE

# 6th post-deployment step - Replace the threedsecurity.conf with value from cvs
tdfilename="threedsecurity.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$tdfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$tdfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${tdfilename}_fromdev
  tdconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$tdfilename
  if [ -f $tdconf ]
  then
    cp $tdconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $tdfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $tdfilename failed, whilst copying from $tdconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $tdfilename, as was unable to find environment-specific variant at location: $tdconf"
  fi
else
  echo "WARNING: No $tdfilename provided from development. Assuming this is rebuild of old CPS (pre threedsecurity.conf)"
fi
echo "Post-Deployment Step 6 complete." | tee -a $LOGFILE

# 7th post-deployment step - Replace the promodiscservice.conf with value from cvs
pcfilename="promodiscservice.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${pcfilename}_fromdev
  pcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$pcfilename
  if [ -f $pcconf ]
  then
    cp $pcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $pcfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $pcfilename failed, whilst copying from $pcconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $pcfilename, as was unable to find environment-specific variant at location: $pcconf"
  fi
else
  echo "WARNING: No $pcfilename provided from development. Assuming this is rebuild of old CPS (pre promodiscservice.conf)"
fi
echo "Post-Deployment Step 7 complete." | tee -a $LOGFILE

# 8th post-deployment step - Replace the carddetails.conf with value from cvs
pcfilename="carddetails.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${pcfilename}_fromdev
  pcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$pcfilename
  if [ -f $pcconf ]
  then
    cp $pcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $pcfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $pcfilename failed, whilst copying from $pcconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $pcfilename, as was unable to find environment-specific variant at location: $pcconf"
  fi
else
  echo "WARNING: No $pcfilename provided from development. Assuming this is rebuild of old CPS (pre carddetails.conf)"
fi
echo "Post-Deployment Step 8 complete." | tee -a $LOGFILE

# 9th post-deployment step - Replace the fraudscreening.conf with value from cvs
pcfilename="fraudscreening.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${pcfilename}_fromdev
  pcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$pcfilename
  if [ -f $pcconf ]
  then
    cp $pcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $pcfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $pcfilename failed, whilst copying from $pcconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $pcfilename, as was unable to find environment-specific variant at location: $pcconf"
  fi
else
  echo "WARNING: No $pcfilename provided from development. Assuming this is rebuild of old CPS (pre fraudscreening.conf)"
fi
echo "Post-Deployment Step 9 complete." | tee -a $LOGFILE

# 10th post-deployment step - Replace the cookielegislation.conf with value from cvs
pcfilename="cookielegislation.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${pcfilename}_fromdev
  pcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$pcfilename
  if [ -f $pcconf ]
  then
    cp $pcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $pcfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $pcfilename failed, whilst copying from $pcconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $pcfilename, as was unable to find environment-specific variant at location: $pcconf"
  fi
else
  echo "WARNING: No $pcfilename provided from development. Assuming this is rebuild of old CPS (pre cookielegislation.conf)"
fi
echo "Post-Deployment Step 10 complete." | tee -a $LOGFILE

# 11th post-deployment step - Replace the countrylist.conf with value from cvs
pcfilename="countrylist.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${pcfilename}_fromdev
  pcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$pcfilename
  if [ -f $pcconf ]
  then
    cp $pcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $pcfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $pcfilename failed, whilst copying from $pcconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $pcfilename, as was unable to find environment-specific variant at location: $pcconf"
  fi
else
  echo "WARNING: No $pcfilename provided from development. Assuming this is rebuild of old CPS (pre countrylist.conf)"
fi
echo "Post-Deployment Step 11 complete." | tee -a $LOGFILE

# 12th post-deployment step - Replace the hcc.conf with value from cvs
pcfilename="hcc.conf"
if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename ]
then
  mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$pcfilename \
       $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${pcfilename}_fromdev
  pcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$pcfilename
  if [ -f $pcconf ]
  then
    cp $pcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
    if [ "$?" = "0" ]
    then
      echo "Substitution of $pcfilename completed successfully"
    else
      declare_error "ERROR: Substitution of $pcfilename failed, whilst copying from $pcconf"
    fi
  else
    declare_error "ERROR: Deployment is now lacking $pcfilename, as was unable to find environment-specific variant at location: $pcconf"
  fi
else
  echo "WARNING: No $pcfilename provided from development. Assuming this is rebuild of old CPS (pre hcc.conf)"
fi
echo "Post-Deployment Step 12 complete." | tee -a $LOGFILE

echo "Done." | tee -a $LOGFILE
