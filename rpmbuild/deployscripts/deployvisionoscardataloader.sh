#!/bin/sh

############################################################################
#
# deployvisionoscardataloader.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the visionoscardataloader webapp
# Usage: sh deployvisionoscardataloader.sh release_name target_name visionoscardataloader.properties [logfile]
#
# Examples: visionoscardataloader.sh 01.01.01 iscapet1 visionoscardataloader-iscapet1.properties 
#
# Assumes the visionoscardataloader rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the visionoscardataloader artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for visionoscardataloader deployments and is amended based on Pluto
#
# Notes:
# - i3/precise not yet added. Add hostnames
#
#
# History follows
# 22/04/10 P Prajwala Created for new brand visionoscardataloader

# @(#) last changed by: $Author$ $Revision$ $Date$
# # $HeadURL$
#
############################################################################

DEPLOYVISIONOSCARLOADER_SH_TEMPLATE=1
deployvisionoscardataloader_sh_svnrev="$Revision$"
DEPLOYVISIONOSCARLOADER_SH_REVISION=`echo $deployvisionoscarloaderdata_sh_svnrev | cut -d" " -f2`


declare_error()
{
  echo " "
  echo "***********************************************************************"
  echo $1
  echo "***********************************************************************"
  echo " "
}



# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`
thishostenv=`echo $thishost | cut -b1-3`



usage="Usage: $0 release_name target_name visionoscarloaderdata_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
VISIONOSCARLOADER_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$VISIONOSCARLOADER_PROPS" -o ! -r "$VISIONOSCARLOADER_PROPS" ]; then
    echo "Couldn't open visionoscardataloader config file $VISIONOSCARLOADER_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"



echo -n "Deploying visionoscardataloader..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi

cd $TARGET_NAME 

# create & populate the required directory structure
VISIONOSCARLOADER_DEPLOY_DIR=$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/visionoscardataloader

mkdir -p ${VISIONOSCARLOADER_DEPLOY_DIR}

# The location into which the webapp will be deployed

echo $VISIONOSCARLOADER_DEPLOY_DIR

cd $VISIONOSCARLOADER_DEPLOY_DIR/

# Create directory structure
mkdir -p logs

unzip $TUI_ISCAPE/releases/$RELEASE_NAME/visionoscardataloader/visionoscardataloader.zip >/dev/null 2>&1
cp $TUI_ISCAPE/releases/$RELEASE_NAME/visionoscardataloader/volstartup.sh .
cp $TUI_ISCAPE/releases/$RELEASE_NAME/visionoscardataloader/volstop.sh .


echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $VISIONOSCARLOADER_DEPLOY_DIR/conf/visionoscardataloader.conf $VISIONOSCARLOADER_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $VISIONOSCARLOADER_DEPLOY_DIR/conf/visionoscardataloader.conf $VISIONOSCARLOADER_PROPS
chmod g+r $VISIONOSCARLOADER_DEPLOY_DIR/conf/visionoscardataloader.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYVISIONOSCARLOADER_SH_TEMPLATE=$DEPLOYVISIONOSCARLOADER_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYVISIONOSCARLOADER_SH_CVSREVISION=$DEPLOYVISIONOSCARLOADER_SH_REVISION" >> $manifestfile
echo "VISIONOSCARLOADER-ISCAPE_PROPERTIES_TAGGED_VISIONOSCARLOADER_TEMPLATE="`cat $VISIONOSCARLOADER_PROPS | grep "^VISIONOSCARLOADER_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp

echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE



# 2nd post-deployment step - As appropriate, copy the StarLicense.properties from cvs
# Determine cvs directory
brfname="StarLicense.properties"
cvs_brfile="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT/visionoscardataloader/$brfname"
if [ -r $cvs_brfile ]
then
    cp -p $cvs_brfile $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/visionoscardataloader/conf/
    echo "  $brfname from cvs for this environment has been deployed."
else
    echo " "
    echo "  *********************************************************************************************"
    echo "  ERROR: This build appears to require a $brfname, yet no such file is provided from CVS"
    echo "  *********************************************************************************************"
    echo " "
fi

echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
