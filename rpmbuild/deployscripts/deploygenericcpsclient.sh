#!/bin/sh
#
# Post-install script to deploy the genericcpsclient webapp
#   as used by rpmbuild (i.e. NOT javarpmbuild)
# Usage: sh deploygenericcpsclient.sh release_name target_name genericcpsclient.properties [logfile]
#
# Examples: deploygenericcpsclient.sh is01.01.01 iscapet1 genericcpsclient-iscapet1.properties powersearch.xsl
#
# Assumes the genericcpsclient rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the genericcpsclient artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in:
#  http://src-repo/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts
# This script is used for wish deployments, and likely for genericcpsclient deployments too. 
#
# History follows
#  pre july - not recorded
# 30/07/07 A Chapman     Check for unused substitution values
# 14/09/07 A Chapman     Add template/revision values for this file, to support CMaycock's manifest plan.
# 29/11/07 A Chapman     Conditionally add the i3-precise listener
# 09/04/07 A Chapman     wish_pat 2nd stream becomes 1st stream, so gets i3
# 08/08/08 A Chapman     Changed to support WISH_TEMPLATE_VERSION
# 14/11/08 A Chapman     Changed to support genericcpsclient
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#

DEPLOYGENERICCPSCLIENT_SH_TEMPLATE=1
deploygenericcpsclient_sh_cvsrev="$Revision$"
DEPLOYGENERICCPSCLIENT_SH_REVISION=`echo $deploygenericcpsclient_sh_cvsrev | cut -d" " -f2`

# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name genericcpsclient_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
GENERICCPSCLIENT_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$GENERICCPSCLIENT_PROPS" -o ! -r "$GENERICCPSCLIENT_PROPS" ]; then
    echo "Couldn't open genericcpsclient config file $GENERICCPSCLIENT_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"



echo -n "Deploying genericcpsclient..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
GENERICCPSCLIENT_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $GENERICCPSCLIENT_DEPLOY_DIR/conf/genericcpsclient.conf $GENERICCPSCLIENT_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $GENERICCPSCLIENT_DEPLOY_DIR/conf/genericcpsclient.conf $GENERICCPSCLIENT_PROPS
chmod g+r $GENERICCPSCLIENT_DEPLOY_DIR/conf/genericcpsclient.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYGENERICCPSCLIENT_SH_TEMPLATE=$DEPLOYGENERICCPSCLIENT_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYGENERICCPSCLIENT_SH_CVSREVISION=$DEPLOYGENERICCPSCLIENT_SH_REVISION" >> $manifestfile
echo "GENERICCPSCLIENT-ISCAPE_PROPERTIES_TAGGED_GENERICCPSCLIENT_TEMPLATE="`cat $GENERICCPSCLIENT_PROPS | egrep "^GENERICCPSCLIENT_TEMPLATE_VERSION=|^WISH_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




#echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE
echo "Deployment performed. " | tee -a $LOGFILE
#echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
