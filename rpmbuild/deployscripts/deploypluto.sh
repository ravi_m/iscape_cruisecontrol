#!/bin/sh

############################################################################
#
# deploypluto.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the pluto webapp
# Usage: sh deploypluto.sh release_name target_name pluto.properties [logfile]
#
# Examples: deploypluto.sh is01.01.01 iscapet1 pluto-iscapet1.properties 
#
# Assumes the pluto rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the pluto artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for pluto deployments and is amended based on TAS
#
# Notes:
# - i3/precise not yet added. Add hostnames
#
#
# History follows
# 06/12/09 P Prajwala     created as copy from thomson script
# 02/11/10 Ramesh Babu   Removed PAT servers list from Precise servers list section as per discussion with Chris on 2/11/2010.
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# # $HeadURL$
#
############################################################################

DEPLOYPLUTO_SH_TEMPLATE=1
deploypluto_sh_svnrev="$Revision$"
DEPLOYPLUTO_SH_REVISION=`echo $deploypluto_sh_svnrev | cut -d" " -f2`


declare_error()
{
  echo " "
  echo "***********************************************************************"
  echo $1
  echo "***********************************************************************"
  echo " "
}



# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`
thishostenv=`echo $thishost | cut -b1-3`



usage="Usage: $0 release_name target_name pluto_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
PLUTO_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$PLUTO_PROPS" -o ! -r "$PLUTO_PROPS" ]; then
    echo "Couldn't open pluto config file $PLUTO_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"



echo -n "Deploying pluto..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
PLUTO_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $PLUTO_DEPLOY_DIR/conf/pluto.conf $PLUTO_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $PLUTO_DEPLOY_DIR/conf/pluto.conf $PLUTO_PROPS
chmod g+r $PLUTO_DEPLOY_DIR/conf/pluto.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYPLUTO_SH_TEMPLATE=$DEPLOYPLUTO_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYPLUTO_SH_CVSREVISION=$DEPLOYPLUTO_SH_REVISION" >> $manifestfile
echo "PLUTO-ISCAPE_PROPERTIES_TAGGED_PLUTO_TEMPLATE="`cat $PLUTO_PROPS | grep "^PLUTO_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp

echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE





# 1st post-deployment step - Conditionally Insert i3-precise listener into server.xml
inject_i3_for_this_instance=true
if [ "$thishost" != "ukpwap07" \
  -a "$thishost" != "ukpwap06" \
  -a "$thishost" != "ukpwap30" \
  -a "$thishost" != "ukpwap31" \
  -a "$thishost" != "ukpwap01" \
  -a "$thishost" != "ukpwap02" ]
then
  inject_i3_for_this_instance=false
fi
this_instance=`echo $TARGET_NAME | sed -e "s/^.*\(.\)$/\1/"`
# Do NOT verify a valid instance here. Buildscripts is not the right place
if [ "$this_instance" != "a" \
  -a "$this_instance" != "b" ]
then
  inject_i3_for_this_instance=false
fi
if [ ! -r /tui/iscape/program/tomcat5.5/apache-tomcat-5.5.12/server/lib/indepthmetric.jar \
  -a ! -r /tui/iscape/program/tomcat5.0/jakarta-tomcat-5.0.28/server/lib/indepthmetric.jar ]
then
  inject_i3_for_this_instance=false
fi

if [ "$inject_i3_for_this_instance" = "true" ]
then 
  echo "i3 JMX Metrics monitoring is being ENABLED" | tee -a $LOGFILE
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 1 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->\)~\1<Listener className=\"com.precise.javaperf.extensions.tomcat.JMXMetricsLoaderListener\"/>~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
else
  echo "i3 JMX Metrics monitoring is not required for this instance" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE


# 2nd post-deployment step - server.xml : the http Connector becomes the xmlrpc Connector
#    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
#      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
#    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
#        sed -e "s~{tomcat.httpport}~{tomcat.xmlrpcport}~" \
#        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
#    rm $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
#echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE


# 3rd post-deployment step - inject unique ID code into iScape/localhost/pluto.xml.
# iscape_hostnumber & thisoneinstancenumber is inherited from iscapectl2.ksh
#   iscape_hosttype=`hostname | cut -b3`
#    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/pluto.xml \
#      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/pluto.xml_org
#    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/pluto.xml_org | \
#        sed -e "s/replace_this_with_unique_id/pluto${iscape_hosttype}${iscape_hostnumber}$thisoneinstancenumber/" \
#        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/pluto.xml
#    rm $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/iScape/localhost/pluto.xml_org
#echo "Post-Deployment Step 3 complete." | tee -a $LOGFILE


# 4th post-deployment step - Replace the datacash.conf with value from cvs
#dcfilename="datacash.conf"
#if [ -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$dcfilename ]
#then
# mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$dcfilename \
#      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/${dcfilename}_fromdev
# dcconf=$TUI_ISCAPE/config/$config_module/$cvs_misc_subdir$LOCAL_ISCAPE_ENVIRONMENT/commonpaymentserver/$dcfilename
# if [ -f $dcconf ]
# then
#   cp $dcconf $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf
#   if [ "$?" = "0" ]
#   then
#     echo "Substitution of $dcfilename completed successfully"
#   else
#     declare_error "ERROR: Substitution of $dcfilename failed, whilst copying from $dcconf"
#   fi
# else
#   declare_error "ERROR: Deployment is now lacking $dcfilename, as was unable to find environment-specific variant at location: $dcconf"
# fi
#else
#  echo "WARNING: No $dcfilename provided from development. Assuming this is rebuild of old PLUTO (pre datacash-conf)"
#fi
#echo "Post-Deployment Step 4 complete." | tee -a $LOGFILE



echo "Done." | tee -a $LOGFILE
