#!/bin/bash

################################################################################################
#
# DESCRIPTION
# This script is for starting up VOL application. It will check if the application is alread up 
# if it is not then, starts otherwise it will exit with proper message.
#
# USAGE
#        ./volstartup.sh
#
# History
# 05/07/2010 1 Prajwala P 	Created for VOL startup
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
################################################################################################


function checkup
{
	echo "Checking if application is up"
	sleep 3

	if [ -f /usr/ucb/ps ]
	then
        	/usr/ucb/ps -auxww | grep -i java | grep -i visionoscardataloader > /dev/null 2>&1
	        if [ $? == 0 ]
       		then
                	echo "VisionOscarLoader is in running mode..."
			exit
		else
			echo "VisionOscarLoader is down, starting now..." 
			return 3
		fi
	else
		echo "ERR:Command does not exist to check if application is running"
		exit 0
	fi
}


checkup

if [ $? == 3 ]
then
	. /isutils/set_local_env.sh > /dev/null 2>&1

	TUI_ISCAPE="/tui/iscape"
	appname="visionoscardataloader"
	routingfile="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT/iscape_routing.config"
	thishost=`hostname`
	thishostnumber=`echo $thishost | cut -b7-8`
	instance=`grep "$thishost;$appname" $routingfile | cut -f3 -d';'`
	instancename=iscape${thishostnumber}${instance}


	JAR_FOLDER="/tui/iscape/releases/current_visionoscardataloader_${LOCAL_ISCAPE_ENVIRONMENT}/${instancename}/${appname}/lib"
	JARS="/tui/iscape/releases/current_visionoscardataloader_${LOCAL_ISCAPE_ENVIRONMENT}/${instancename}/${appname}/conf"
	JAVA="/tui/iscape/program/java1.6/jdk1.6.0_18/bin/java"

	printf "About to issue command... \n$JAVA -cp $JAR_FOLDER/visionoscardataloader.jar:$JARS:$JAR_FOLDER/* com.tui.uk.scheduler.VOLScheduler\n\n"

	$JAVA -cp $JAR_FOLDER/visionoscardataloader.jar:$JARS:$JAR_FOLDER/* com.tui.uk.scheduler.VOLScheduler  > /dev/null 2>&1 &
	
	sleep 5

	checkup
	
fi	
