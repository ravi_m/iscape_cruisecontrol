#!/bin/bash

################################################################################################
#
# DESCRIPTION
# This script is  to stop VisionOscarLoader application
#
# USAGE
#        ./volstartup.sh
#
# History
# 05/07/2010 1 Prajwala P       Created for VOL startup
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
################################################################################################

function check
{
	if [ -f /usr/ucb/ps ]
	then
        	/usr/ucb/ps -auxww | grep -i java | grep -i visionoscardataloader > /dev/null 2>&1
	        if [ $? == 0 ]
       		then
			echo "Checking if application in running mode before shutting down..."
			sleep 3
			return 2
		else
			echo "VisionOscarLoader is down.."
		fi 
	else
		echo "ERR:Command does not exist to check the application"
	fi
}


check

if [ $? == 2 ]
then
	. /isutils/set_local_env.sh > /dev/null 2>&1

	TUI_ISCAPE="/tui/iscape"
	appname="visionoscardataloader"
	routingfile="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT/iscape_routing.config"
	thishost=`hostname`
	thishostnumber=`echo $thishost | cut -b7-8`
	instance=`grep "$thishost;$appname" $routingfile | cut -f3 -d';'`
	instancename=iscape${thishostnumber}${instance}

	LOG_FOLDER="/tui/iscape/releases/current_visionoscardataloader_${LOCAL_ISCAPE_ENVIRONMENT}/${instancename}/$appname/logs"
	echo $LOG_FOLDER
	echo "Shutting down VisionOscarLoader by creating flag file..."
	touch $LOG_FOLDER/volshutdownflag.txt
	
	sleep 5
	
	check
fi


