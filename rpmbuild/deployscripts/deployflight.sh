#!/bin/sh
#
# Post-install script to deploy the flight webapp
#   as used by rpmbuild (i.e. NOT javarpmbuild)
# Usage: sh deployflight.sh release_name target_name flight.properties [logfile]
#
# Examples: deployflight.sh is01.01.01 iscapet1 flight-iscapet1.properties powersearch.xsl
#
# Assumes the flight rpm has already been rpm-installed
# into /tui/iscape/releases/release_name.
# Deploys the callcentre artifacts into the deployment directory
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# History follows
#  pre july - not recorded
# 30/07/07 A Chapman     Check for unused substitution values
#

TUI_ISCAPE=/tui/iscape
. $TUI_ISCAPE/program/setenv.sh

usage="Usage: $0 release_name target_name flight_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
FLIGHT_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage
    exit 1
fi

if [ ! -f "$FLIGHT_PROPS" -o ! -r "$FLIGHT_PROPS" ]; then
    echo "Couldn't open callcentre config file $FLIGHT_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi

echo -n "Deploying flight..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $DEPLOY_DIR/conf/flight.conf $FLIGHT_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $DEPLOY_DIR/conf/flight.conf $FLIGHT_PROPS
chmod g+r $DEPLOY_DIR/conf/flight.conf

echo "Done." | tee -a $LOGFILE
