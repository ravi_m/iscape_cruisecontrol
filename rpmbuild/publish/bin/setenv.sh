#!/bin/bash

#======================================================================
# Tomcat6.0.x version catalina.sh file clears the CLASSPATH.
# So we need to set the classpath explicitly.
# "$CATALINA_BASE"/bin/setenv.sh
# For reference please check line numbers from 110 to 118 lines.
#======================================================================

CLASSPATH=$CATALINA_BASE
export CLASSPATH

