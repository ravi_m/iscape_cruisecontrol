<!-- DO NOT amend any of the comments below containing :
	'INSERT BRAC SECURITY STANZA HERE' 
or 	'INSERT I3 PRECISE LISTENER1 HERE'
   They are relied upon by an automated process when deploying to Unix environments -->

<!-- server.xml.fcfalcon              -->
<!-- iScape Server Configuration File -->
<!-- Version 1.0 - October 5th 2006   -->
<!-- Simon Jones                      -->
<!--                                  -->
<!-- Master version of this file is located at: http://src-repo/svn/iscape_cruisecontrol/trunk/rpmbuild/publish/conf/  -->
<!--                                  -->
<!-- History:                         -->
<!-- 28/11/08 A Chapman           Variant for fcfalcon, as requested by Roger Magi Fri 28/11/2008 09:54 -->
<!--                                  -->
<!--                                  -->
<!--                                  -->

<!--

Runtime parameters are used to allow different configurations 
in different environments. The parameters are setup by the 
startup script prior to running the JVM, as follows:

set JAVA_OPTS=-Dtomcat.httpport=8080 -Dtomcat.shutdownport=8005 -Dtomcat.ajpport=8009 -Dtomcat.address=0.0.0.0 -Dtomcat.instance=iscape1  

The parameters are:

tomcat.httpport=8080
This is the HTTP Port number. Defaults to 8080. If multiple instances
are to be run on a single machine, then either Virtual IP addresses 
must be used, or different port numbers must be used.

tomcat.shutdownport=8005 
This is the port number that will be used for the tomcat Shutdown.

tomcat.ajpport=8009 
This is the AJP port number. This is used by Apache mod_jk to 
send requests to tomcat. Browsers cannot connect to this port 
directly, which is why it's a good idea to have an HTTP port too.

tomcat.address=0.0.0.0 
Tomcat will bind onto this address when it starts. If multiple
instances are required then either different ports must be used, or
different IP addresses must be used. This allows a specific IP 
address to be used. If 0.0.0.0 is specified, all the available 
IP addresses (physical and virtual) will be bound.

tomcat.instance=iscape1  
mod_jk requires the jvmRoute to be set to a different value for each 
tomcat instance. This is not 'clustering', but is required for mod_jk stickiness.
If this is not set correctly, mod_jk will not be able to direct requests
to the tomcat instance that has the users session data.

-->

<Server address="${tomcat.address}" port="${tomcat.shutdownport}" shutdown="SHUTDOWN">

  <Listener className="org.apache.catalina.core.AprLifecycleListener" />
  <Listener className="org.apache.catalina.mbeans.ServerLifecycleListener" />
  <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
  <Listener className="org.apache.catalina.storeconfig.StoreConfigLifecycleListener"/>
    <!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->
    <!-- End of INSERT I3 PRECISE LISTENER1 HERE -->

  <GlobalNamingResources>
    <Resource name="UserDatabase" auth="Container"
              type="org.apache.catalina.UserDatabase"
       description="User database that can be updated and saved"
           factory="org.apache.catalina.users.MemoryUserDatabaseFactory"
          pathname="conf/tomcat-users.xml" />
  </GlobalNamingResources>

  <!-- Define the iScape Tomcat Stand-Alone Service -->
  <Service name="Catalina">

    <!-- Default non-SSL HTTP/1.1 Connector on port 8080 -->
    <Connector address="${tomcat.address}" port="${tomcat.httpport}" maxHttpHeaderSize="8192"
               maxThreads="40" minSpareThreads="5" maxSpareThreads="20"
               enableLookups="false" acceptCount="100"
               connectionTimeout="600000" disableUploadTimeout="true" />

    <!-- Define an AJP 1.3 Connector on port 8009 -->
    <Connector address="${tomcat.address}" port="${tomcat.ajpport}" 
               minSpareThreads="100" maxSpareThreads="50" maxThreads="750" enableLookups="false" 
               emptySessionPath="true" backlog="20" connectionTimeout="600000" protocol="AJP/1.3" />

    <Engine name="iScape" defaultHost="localhost" jvmRoute="${tomcat.instance}">         

      <!-- Start of INSERT BRAC SECURITY STANZA HERE -->
      <!-- End of INSERT BRAC SECURITY STANZA HERE -->

      <Host name="localhost" appBase="webapps" unpackWARs="true" autoDeploy="false"
 		      xmlValidation="false" xmlNamespaceAware="false">

        <Valve className="org.apache.catalina.valves.AccessLogValve"
                 directory="logs"  prefix="iscape_access_log." suffix=".txt"
                 pattern="combined" resolveHosts="false"/>

      </Host>

    </Engine>

  </Service>

</Server>
