#!/bin/bash

############################################################################
#
# tomcat.sh - as used by most iScape brands
#
# SYNOPSIS   sh tomcat.sh [i3] instance_name [tomcat args]
# e.g. to start iscapet1 in the same window: sh tomcat.sh iscapet1 run
#
# If the optional i3 parameter is provided, i3 functionality is also started
#
# Set the current directory to be the
# directory of this script. This is used
# as catalina_base, and the log files are
# relative to this directory. 
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/publish
#
# This is believed to be the MASTER tomcat.sh that is included in EVERY iscape build
#
#
# History:
# 29/02/08 01 A Chapman     Added below cvs tags to assit in identifying revisions & location
# 29/02/08 02 A Chapman     Added proxy-if-uk conditional code for brac. However, will affect ALL Http* accesses for all webapps.
#                           http* will not affect MYSQL, Mailserver, Oracles, but would affect iscape-to-xdist (for fhpi, etc)
#                           Hence add 10.40.* & 10.145.*
# 17/03/08 03 A Chapman     Whilst fixing brac, this is breaking wish. Hence make change specific to brac
# 28/08/08 04 A Chapman     Add additional switch (occasional crash problem) as requested by Yasin email Wed 27/08/2008 15:14  XX:ReservedCodeCacheSize=96m
# 06/01/10 05 A Chapman     Add -Xss256K to JAVA_OPTS as Yasin specified Wed 06/01/2010 11:34
# 07/02/11 06 Ramesh Babu   Add Dsun.net.inetaddr.ttl=30 to JAVA_OPTS to refresh inet address to get reCAPTCHA IP address.
#
#
# @(#) last changed by: $Author: xxxx $ $Revision: xxxx $ $Date: xxxx $
# $HeadURL: xxxx $
#
############################################################################

cd `dirname "$0"`

. /tui/iscape/program/setenv.sh

# Check for optional i3 param. If found, eat it & take appropriate actions
if [ "$1" == "i3" ]
then
  i3selected="y"
  ISCAPE_JVM=$2
  echo "Setting parameters such that i3 monitoring will be active"
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/tui/precise/v75/products/j2ee/lib
  export LD_LIBRARY_PATH
  I3_OPTS="-Xbootclasspath/p:/tui/precise/v75/products/j2ee/etc/apl/jvms/java-vm-$ISCAPE_JVM/lib/rt.jar -Xbootclasspath/p:/tui/precise/v75/products/j2ee/lib/indepthj2eeboot.jar -Xbootclasspath/p:/tui/precise/v75/products/j2ee/classes -Dindepth.j2ee.server=/tui/precise/v75/products/j2ee/config/$ISCAPE_JVM"
  shift
fi
ISCAPE_JVM=$1
export ISCAPE_JVM


JAVA_HOME=$JAVA15 export JAVA_HOME
PATH=$JAVA_HOME/bin:/usr/bin:/bin export PATH
CATALINA_HOME=$TOMCAT55 export CATALINA_HOME
CATALINA_BASE="`pwd`" export CATALINA_BASE

JPDA_TRANSPORT="dt_socket" export JPDA_TRANSPORT
JPDA_ADDRESS="8000" export JPDA_ADDRESS

# obtain values for instance_name: ip address; http, shutdown, ajp, jmx ports; 
# from environment variables set in setenv.sh
ipaddrname=$1"ipaddress"
ipaddr=${!ipaddrname}
httpportname=$1"httpport"
httpport=${!httpportname}
if [ "$httpport" = "" ]
 then httpport=8080
fi
shutdownportname=$1"shutdownport"
shutdownport=${!shutdownportname}
if [ "$shutdownport" = "" ]
 then shutdownport=8005
fi
ajpportname=$1"ajpport"
ajpport=${!ajpportname}
if [ "$ajpport" = "" ]
 then ajpport=8009
fi
jmxportname=$1"jmxport"
jmxport=${!jmxportname}

JAVA_OPTS="-showversion -server -Xms${INIT_HEAP_SIZE_ISCAPE-256M} -Xmx${MAX_HEAP_SIZE_ISCAPE-256M} -XX:MaxPermSize=${MAX_PERM_SIZE_ISCAPE-96M} \
 -XX:ReservedCodeCacheSize=96m -Xss256K \
 -Dsun.net.client.defaultConnectTimeout=30000 -Dsun.net.client.defaultReadTimeout=180000 \
 -Dtomcat.httpport=$httpport -Dtomcat.shutdownport=$shutdownport -Dtomcat.ajpport=$ajpport  \
 -Dtomcat.address=$ipaddr -Dsun.net.inetaddr.ttl=30 -Dtomcat.instance=$1"  

if [ "`hostname | cut -b1-2`" != "uk" -a -f ../../deployscripts/deploybrac.sh  ]
then
  echo "This IS a UK brac deployment, so applying squid setup"
  # Only provide Squid proxy details for non-hannover environments
  if [ "$SQUID_PROXY_HOST" = "" ]; then
        echo "The SQUID_PROXY_HOST environment variable is not defined."
        echo "This should point to the host name or ip address, where squid proxy server is running"
        exit 1
  fi

  if [ "$SQUID_PROXY_PORT" = "" ]; then
        echo "The SQUID_PROXY_PORT environment variable is not defined."
        echo "This should give the port number on which the squid proxy server is operating"
        exit 1
  fi

  JAVA_OPTS="$JAVA_OPTS \
 -Dhttp.proxyHost=$SQUID_PROXY_HOST \
 -Dhttp.proxyPort=$SQUID_PROXY_PORT \
 -Dhttp.nonProxyHosts=localhost|62.190.109.53|62.184.215.220|10.147.13.84|prd-ctshoteluk.tui.de|10.147.10.7|10.40.*|10.145.* \
 -Dhttps.proxyHost=$SQUID_PROXY_HOST \
 -Dhttps.proxyPort=$SQUID_PROXY_PORT \
 -Dhttps.nonProxyHosts=localhost|62.190.109.53|62.184.215.220|10.147.13.84|prd-ctshoteluk.tui.de|10.147.10.7|10.40.*|10.145.* "
else
  echo "NOT a UK brac deployment, so not applying squid setup"
fi
export JAVA_OPTS

shift

#
# Only setup Management options if the server is starting.
# The 'stop' command will fail if the port is
# already in use!
# TODO: Add security to monitoring and management
#
if [ "$1" == "start" -o "$1" == "run" ]
then
    export CATALINA_OPTS="-Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.port=$jmxport"
  if [ "$i3selected" == "y" ]
  then
    export CATALINA_OPTS=${CATALINA_OPTS}" "${I3_OPTS}
  fi
fi

$CATALINA_HOME/bin/catalina.sh $*
