#!/bin/bash
#
# Script to build project artifacts from cvs label
#
# USAGE
#        buildfromcvs.sh PROJECT CVSLABEL
# PROJECT one of thomson, cmsdev, xdist, brac
# EXAMPLE
#        ./buildfromcvs.sh thomson is01_00_02
# AUTHOR
#        Julia Dain, TUI UK

function buildwebdevproject
{
  export JAVA_HOME=/home/bob/tools/jdk1.5.0_09

  pushd releasecheckout
  
  webdevproject=$1
  
  # checkout build.web.props for the build
  cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/iscape_cruisecontrol update -d -r $cvslabel rpmbuild/releasecheckout/build.web.props
  cp rpmbuild/releasecheckout/build.web.props .

  # checkout project sources
  cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel webdev/build.xml || exit 2
  cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel webdev/global || exit 3 
  cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel webdev/webapps/$webdevproject || exit 4

  # build the project
  ( cd webdev; ant -Dprojname=$webdevproject clean; ant -Dprojname=$webdevproject -Dversion=$cvslabel ) || exit 5 

  # tidy up
  rm build.web.props

  # move artifacts to ftp area
  artifacts=/srv/ftp/pub/artifacts/webdev-webapps-$webdevproject/cvs-$cvslabel
  mkdir -p $artifacts
  echo "Copying artifacts to $artifacts"
  cp webdev/webapps/$webdevproject/conf/$webdevproject.conf $artifacts
  mv $webdevproject.war $artifacts
  
  popd
}

project=$1
cvslabel=$2

if [ -z "$project" -o -z "$cvslabel" ]; then  
  echo "USAGE: $0 PROJECT CVSLABEL" ; exit
fi

case $project in
  brac)
    #buildwebdevproject thomson  # we are not including thomson yet as currently incompatible with brac
    buildwebdevproject brac
    exit
    ;;
    
  flight)
    buildwebdevproject flight
    exit
    ;;
    
  thomson)
    buildwebdevproject thomson
    exit
    ;;
    
  cmsdev)
    cd releasecheckout

    # checkout cmsdev
    cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel cmsdev/mime.article || exit 2
    cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel cmsdev/thomson.co.uk || exit 3
    cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel cmsdev/brac || exit 3
    cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel cmsdev/thomsonfly.com || exit 3
    cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel cmsdev/common || exit 3
    # zip it up - excluding CVS files
    zip cmsdev.zip -r cmsdev -x "*/CVS/*"  
    # move artifact to ftp area
    artifacts=/srv/ftp/pub/artifacts/cmsdev/cvs-$cvslabel
    mkdir -p $artifacts
    echo "Copying artifacts to $artifacts"
    mv cmsdev.zip $artifacts
    exit
    ;;    
  
  xdist)
      cd releasecheckout
      
      # checkout the sources
      cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -d -r $cvslabel xdistdev/xdist || exit 2
      
      # remove all debug logging from xdist prior to build
      ../disablexdistdebug.sh
      
      # no build required, as detokenizing and compiling the rules will be done at deploy time
      # just zip up the artifact - excluding CVS files
      cd xdistdev/xdist
      zip xdist-tokenized.zip -r .  -x "*/CVS/*" "amadeus/AmadeusJNI.zip" "amadeus/*/API_*"
      # move artifact to ftp area
      artifacts=/srv/ftp/pub/artifacts/xdistdev-xdist/cvs-$cvslabel
      mkdir -p $artifacts
      echo "Copying artifacts to $artifacts"
      mv xdist-tokenized.zip $artifacts
      exit
      ;;
  
  *) echo $usage; 
     echo "Invalid project" ${project} "- valid projects are thomson, cmsdev, xdist"; 
     exit 1
     ;;
esac
