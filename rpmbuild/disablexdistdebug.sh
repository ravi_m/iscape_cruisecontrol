#!/bin/bash
#
# Script to change all logging=true to logging=false in xdist to disable debugging.
#
# USAGE
#        disablexdistdebug.sh
# AUTHOR
#        Richard Hands, TUI UK

for messageset in `find xdistdev/xdist -name MessageSet.xml`
do
        cp $messageset $messageset.bak
        echo ^M >> $messageset
        sed s/logging=\"true\"/logging=\"false\"/g $messageset > $messageset.new
        cp $messageset.new $messageset
done
