#!/bin/sh -xv
#
# NAME 
#  rpmbuild.sh - build an rpm for a CruiseControl or CVS project
#
# SYNOPSIS
#  rpmbuild.sh PROJECT VERSION RELEASE [cruise|svn] [cruise_build|svn_tag]
#
# DESCRIPTION
#   Create an rpmbuild spec for PROJECT, run an rpmbuild, move the rpm to the ftp area.
#   Package either the specified Cruise build artifacts or the latest build artifacts into the rpm.
#   PROJECT must be one of thomson, cmsdev, xdist, brac.
#   VERSION should be in the approved TUI format mm.nn where mm gives the major version and nn the minor.
#   RELEASE should be in the approved TUI format rr where rr gives the release (build) number for the version.
#   [cruise|svn] must be one of cruise, svn. This indicates whether the rpm will be built from Cruise artifacts
#   or from a CVS revision tag.
#   [cruise_build|svn_tag] For cruise, this is an optional Cruise build number specifying the Cruise artifacts;
#   if it is present but the Cruise log file cannot be found,
#   the rpm will not be built; if it is not present, the latest cruise build artifacts will be used.
#   For svn, this is a mandatory CVS revision tag (label) specifying which
#   CVS sources will be checked out and built.
#
#   Warning - this script relies on CruiseControl file system structure not changing.
#
#   Examples 
#     to build rpm for thomson version 01.01, release 02, from Cruise build 714: 
#       rpmbuild.sh thomson 01.01 02 cruise 714
#     to build rpm for cmsdev version 01.00, release 03, from Cruise latest build:
#       rpmbuild.sh cmsdev 01.00 03 cruise
#     to build rpm for xdist version 01.00, release 04, from CVS tag is01_00_04:
#       rpmbuild.sh xdist 01.00 04 svn is01_00_04
#
# AUTHOR 
#   Julia Dain   TUI UK   2006

JAVA_HOME=$HOME/tools/jdk1.5.0_09
PATH=$HOME/tools/apache-ant-1.6.5/bin:$PATH

export PATH JAVA_HOME

# argument checking
usage="Usage: rpmbuild.sh project version release [cruise|svn] [cruise_build|svn_tag]"
echo "***********************************************************************************************************************"
echo "THIS IS FOR THE OLD CODE REPOSITORY.  PLEASE ENSURE YOU INTENDED TO RUN THIS RPM SCRIPT AND NOT THE ONE IN NEWRPMBUILD!"
echo "***********************************************************************************************************************"
# Prevent two rpmbuilds running simultaneously,
# as there may be a clash of checkouts.
lockfile="/tmp/rpmbuild.lock"

if [ -f ${lockfile} ]; then
    echo "Can't run two rpmbuilds at once."
    echo "Is there another rpmbuild process running (PID `cat ${lockfile}`)?"
    echo "Please check, and if no rpmbuild is running, manually remove ${lockfile}"
    exit 1
fi

# Clean up the lock file in the event of an interrupt.

cleanup_lockfile() {
    rm -f ${lockfile}
}

trap cleanup_lockfile INT QUIT 

case $1 in
  --help|-h) echo $usage; echo "Examples: rpmbuild.sh thomson latest build cruise; rpmbuild.sh xdist 01.00 04 svn is01_00_04"; exit 0;;
  *) ;;
esac

case $# in
  5|6) ;;
  *) echo $usage; exit 1;;
esac
   
project=$1
version=$2
release=$3
source=$4
tag=$5

case ${project} in
  thomson) ;;
  cmsdev) ;;
  xdistbrac) ;;
  xdistwish) ;;
  brac) ;;
  flight) ;;
  *) echo $usage; 
     echo "Invalid project" ${project} "- valid projects are thomson, cmsdev, xdistwish, xdistbrac, brac, flight"; 
     cleanup_lockfile;
     exit ;;
esac

# Echo the process id into the lock file to aid identification of this process in the event of a problem
# NOTE: Whenever you exit from this point onwards, you must remember to call cleanup_lockfile to make
# sure that the lock file is removed.
echo $$ > ${lockfile}

case ${source} in
  cruise)
    # get the required cruise build log filename
    case $# in
      4)
         # latest build is required - get the filename of its log
         log=`ls -t /home/bob/cc-work/logs/*${project}/log*.xml | grep _build | head -1`     
         ;;
      5)
         # build label specified as arg4 - get the filename of its log
         log=`ls -t /home/bob/cc-work/logs/*${project}/log*.xml | grep _build-$tag | head -1`
         if test $log
           then  :
           else  echo "Could not find cruise log for" ${project} "build" $tag; cleanup_lockfile ; exit 1
         fi
         ;;
    esac

    # extract the date from the log filename 
    cruisebuilddate=`echo $log | sed -e "s/.*log// ; s/L.*//"`
    # extract the label from the log filename
    cruisebuildlabel=`echo $log | sed -e "s/.*_build-// ; s/.xml//"`
    buildtype="cruise build"
    ;;
    
  svn) echo svn build
       buildtype="svn build"
       cruisebuildlabel=$tag
       cruisebuilddate=svn-$tag  # fix to get the correct svn artifacts directory for rpmbuild ftp
       if [ $project == "xdistbrac" ]
       then
           echo "about to issue cmd : ./buildfromsvnbrac.sh $project $tag"
           ./buildfromsvnbrac.sh xdist $tag  || { echo "svn build failed" ; cleanup_lockfile ; exit 1 ; } 
           project=xdist
       elif [ $project == "xdistwish" ]
       then
           echo "about to issue cmd : ./buildfromsvn.sh $project $tag"
           ./buildfromsvn.sh xdist $tag  || { echo "svn build failed" ; cleanup_lockfile ; exit 1 ; } 
           project=xdist
       else
           echo "about to issue cmd : ./buildfromsvn.sh $project $tag"
           ./buildfromsvn.sh $project $tag  || { echo "svn build failed" ; cleanup_lockfile ; exit 1 ; } 
       fi
       ;;
       
  *)  echo $usage;
      echo "Invalid source" ${source} "- valid sources are cruise and svn";
      cleanup_lockfile ;
      exit ;;
esac

# create the rpm spec
filename=${project}-${version}-${release}
echo "Creating rpmbuild spec" ${filename}.spec "for" ${project} $buildtype $cruisebuildlabel
ant -verbose -Dproject=${project} -Dversion=${version} -Drelease=${release} \
-Drpmbuilddate="`date +"%a %b %e %Y"`" -Drpmbuilddatetime="`date`" \
-Dcruisebuilddate=$cruisebuilddate -Dcruisebuildlabel="$buildtype $cruisebuildlabel" \
-f build-spec.xml

# build the rpm
echo "Building rpm"
rpmbuild -vv -bb --target noarch SPECS/${filename}.spec >buildoutput/${filename}.out 2>&1

# move the rpm to the ftp area
ftp=/srv/ftp/pub/rpms
echo "Moving" ${filename}.noarch.rpm "to" ${ftp}
mv /usr/src/packages/RPMS/noarch/${filename}.noarch.rpm /srv/ftp/pub/rpms

cleanup_lockfile

