#!/bin/bash -xv
#
# Script to build project artifacts from cvs label
#
# USAGE
#        buildfromcvs.sh PROJECT CVSLABEL
# PROJECT one of thomson, cmsdev, xdist, brac
# EXAMPLE
#        ./buildfromcvs.sh thomson is01_00_02
# AUTHOR
#        Julia Dain, TUI UK

function buildwebdevproject
{
  export JAVA_HOME=/home/bob/tools/jdk1.5.0_09

  pushd releasecheckout
  
  webdevproject=$1
  
  # checkout build.web.props for the build
  echo "$pwd"
  echo "Checking out build.web.apps file"
  #svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/tags/$cvslabel/rpmbuild/releasecheckout/build.web.props ./build.web.props
  svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/releasecheckout/build.web.props ./build.web.props

  # checkout project sources
  echo "Checking out build.xml, global and webapps"
  svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/webdev/build.xml webdev/build.xml || exit 2
  svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/webdev/global webdev/global || exit 3
  svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/webdev/webapps/$webdevproject webdev/webapps/$webdevproject || exit 4

  export ANT_OPTS=-XX:+UseParallelGC
  export JAVA_OPTS=-XX:+UseParallelGC
  # build the project
  echo "Ant command "
  ( cd webdev; ant -Dprojname=$webdevproject clean; ant -Dprojname=$webdevproject -Dversion=$cvslabel ) || exit 5 

  # tidy up
  rm build.web.props

  # move artifacts to ftp area
  artifacts=/srv/ftp/pub/artifacts/webdev-webapps-$webdevproject/svn-$cvslabel
  mkdir -p $artifacts
  echo "Copying artifacts to $artifacts"
  cp webdev/webapps/$webdevproject/conf/$webdevproject.conf $artifacts
  mv $webdevproject.war $artifacts
  
  popd
}

project=$1
cvslabel=$2

if [ -z "$project" -o -z "$cvslabel" ]; then  
  echo "USAGE: $0 PROJECT CVSLABEL" ; exit
fi

case $project in
  brac)
    #buildwebdevproject thomson  # we are not including thomson yet as currently incompatible with brac
    buildwebdevproject brac
    exit
    ;;
    
  flight)
    buildwebdevproject flight
    exit
    ;;
    
  thomson)
    buildwebdevproject thomson
    exit
    ;;
    
  cmsdev)
    cd releasecheckout

    # AC 5/11/08 - ensure dir exists, to avoid error
    mkdir -p cmsdev

    # checkout cmsdev
    svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/cmsdev/mime.article cmsdev/mime.article || exit 2
    svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/cmsdev/thomson.co.uk cmsdev/thomson.co.uk || exit 3
    # AC 5/11/08 - add thomsonfly.com
    svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/cmsdev/thomsonfly.com cmsdev/thomsonfly.com || exit 3

    #svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/cmsdev/brac cmsdev/brac || exit 4
    #svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/cmsdev/thomsonfly.com cmsdev/thomsonfly.com || exit 5
    #svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/cmsdev/common cmsdev/common || exit 6
    # zip it up - excluding CVS files
    zip cmsdev.zip -r cmsdev 
    # move artifact to ftp area
    artifacts=/srv/ftp/pub/artifacts/cmsdev/svn-$cvslabel
    mkdir -p $artifacts
    echo "Copying artifacts to $artifacts"
    mv cmsdev.zip $artifacts
    exit
    ;;    
  
  xdist)
      cd releasecheckout
      
      # checkout the sources
      svn --force export http://10.145.36.205/svn/development/tags/$cvslabel/xdistdev/xdist xdist || exit 2
      
      # remove all debug logging from xdist prior to build
      ../disablexdistdebug.sh
      
      # no build required, as detokenizing and compiling the rules will be done at deploy time
      # just zip up the artifact - excluding CVS files
      cd xdist
      zip xdist-tokenized.zip -r . -x "*/\.svn/*" "amadeus/AmadeusJNI.zip" "amadeus/*/API_*"
      # move artifact to ftp area
      artifacts=/srv/ftp/pub/artifacts/xdistdev-xdist/svn-$cvslabel
      mkdir -p $artifacts
      echo "Copying artifacts to $artifacts"
      mv xdist-tokenized.zip $artifacts
      exit
      ;;
  
  *) echo $usage; 
     echo "Invalid project" ${project} "- valid projects are thomson, cmsdev, xdist"; 
     exit 1
     ;;
esac
