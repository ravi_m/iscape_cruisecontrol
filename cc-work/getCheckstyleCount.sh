#!/bin/sh
# Written by Paul Jefferies - 05/06/2007
# Params: 
#	$1 - The directory report to get the checkstyle report from
#	$2 - The project to get the checkstyle i.e. shared, thomson, brac etc.
# Gets the checkstyle count from the compiled reports within the ./reports directory by interogating the overview-summary.html file.

DIR=$1
PROJECT=$2
CCWORKDIR=~/cc-work
REPORTDIR=$CCWORKDIR/reports
REPORTFILE=$REPORTDIR/$DIR/$PROJECT'chart-checkstyle/overview-summary.html'

if test -f $REPORTFILE; then
        CHECKSTYLE_ERRORS=`cat $REPORTFILE | grep -i "checkstyle" | awk '{ print $5 }' FS="<td>" | awk '{ print $1 }' FS="</td>" RS="\n\n+"`	
	echo `date` - $CHECKSTYLE_ERRORS in $REPORTFILE >> $CCWORKDIR/checkstyleErrors.log
	echo $CHECKSTYLE_ERRORS
else
	echo Unknown report file: $REPORTFILE >> $CCWORKDIR/checkstyleErrors.log
	echo 9999
fi

