#!/bin/bash

# Chris Lilley
#
# This script is called by /etc/init.d/cruisecontrol to start CruiseControl on boot. 
# You can call it by hand if you want to, but it's probably better to use cc.sh
#

export JAVA_HOME=~/tools/jdk1.5.0_09
export PATH=$JAVA_HOME/bin:$PATH
export CCDIR=~/tools/cruisecontrol-2.5/main/

export CC_OPTS="-Dhttp.proxyHost=proxy -Dhttp.proxyPort=8080"

~/tools/cruisecontrol-2.5/main/bin/cruisecontrol.sh -jmxport 8000 -rmiport 54321 &>cc.log
